# MsgPack-b: for Haskell

The goal is to be as convenient to use as
[Aeson](http://hackage.haskell.org/package/aeson), but with all the
additional potential for speed and compactness that
[msgpack](http://msgpack.org/) affords.

## MsgPack Objects

Objects are described by the following data type:

```haskell
data Value
  = PInt !Word8
  | Nil
  | Bool !Bool
  | Float !Float
  | Double !Double
  | W8 !Word8
  | W16 !Word16
  | W32 !Word32
  | W64 !Word64
  | I8 !Int8
  | I16 !Int16
  | I32 !Int32
  | I64 !Int64
  | Binary !Len !B.ByteString
  | Text !Len !Text
  | Array !(V.Vector Value)
  | Map !(HM.HashMap Text Value)
  | FixExt !Len !Type !B.ByteString
  | Ext !Type !B.ByteString
  | NInt !Int8 deriving Show
```
