{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Data.MsgPack.Types where

import Control.Applicative
import Control.Monad (mzero)
import Data.Bits
import qualified Data.ByteString as B
import qualified Data.HashMap.Lazy as HM
import Data.Hashable
import Data.Int
import Data.Serialize
import Data.Text (Text)
import Data.Text.Encoding (encodeUtf8, decodeUtf8)
import qualified Data.Vector as V
import Data.Word

newtype Type = Type Word8 deriving (Show, Serialize)
newtype Len = Len Int deriving Show

data Value
  = PInt !Word8
  | Nil
  | Bool !Bool
  | Float !Float
  | Double !Double
  | W8 !Word8
  | W16 !Word16
  | W32 !Word32
  | W64 !Word64
  | I8 !Int8
  | I16 !Int16
  | I32 !Int32
  | I64 !Int64
  | Binary !Len !B.ByteString
  | Text !Len !Text
  | Array !(V.Vector Value)
  | Map !(HM.HashMap Text Value)
  | FixExt !Len !Type !B.ByteString
  | Ext !Type !B.ByteString
  | NInt !Int8 deriving Show

instance (Serialize a) => Serialize (V.Vector a) where
  put v = put $ V.toList v
  get = V.fromList <$> get

instance (Eq k, Hashable k, Serialize k, Serialize v) =>
         Serialize (HM.HashMap k v) where
  put m = put $ HM.toList m
  get = HM.fromList <$> get

instance Serialize Text where
  put = put . encodeUtf8
  get = decodeUtf8 <$> get

instance Serialize Value where
  get = do
    b' <- getWord8
    case b' of
      0xc0 -> return Nil
      0xc1 -> mzero  -- reserved
      0xc2 -> return $ Bool False
      0xc3 -> return $ Bool True
      0xc4 -> mkBinary getWord8
      0xc5 -> mkBinary getWord16be
      0xc6 -> mkBinary getWord32be
      0xc7 -> mkExt getWord8
      0xc8 -> mkExt getWord16be
      0xc9 -> mkExt getWord32be
      0xca -> Float <$> getFloat32be -- float 32
      0xcb -> Double <$> getFloat64be -- float 64
      0xcc -> W8 <$> get -- word 8
      0xcd -> W16 <$> getWord16be -- word 16
      0xce -> W32 <$> getWord32be -- word 32
      0xcf -> W64 <$> getWord64be -- word 64
      0xd0 -> I8 <$> get -- int 8
      0xd1 -> (I16 . fromIntegral) <$> getWord16be -- int 16
      0xd2 -> (I32 . fromIntegral) <$> getWord32be -- int 32
      0xd3 -> (I64 . fromIntegral) <$> getWord64be -- int 64
      0xd4 -> mkFixExt 1
      0xd5 -> mkFixExt 2
      0xd6 -> mkFixExt 4
      0xd7 -> mkFixExt 8
      0xd8 -> mkFixExt 16
      0xd9 -> mkStr getWord8
      0xda -> mkStr getWord16be
      0xdb -> mkStr getWord32be
      0xdc -> mkArray getWord16be
      0xdd -> mkArray getWord32be
      0xde -> mkMap getWord16be
      0xdf -> mkMap getWord32be
      _ -> range b'
    where range b
            | b >= 0x00 && b <= 0x7f = return $ PInt b
            | b >= 0x80 && b <= 0x8f = Map <$> get   -- fixmap
            | b >= 0x90 && b <= 0x9f = Array <$> get -- fixarray
            | b >= 0xa0 && b <= 0xbf = do
              let n = fromIntegral $ b `shiftR` 3 :: Int
              Text (Len n) <$> (decodeUtf8 <$> getBytes n) -- fixstr
            | b >= 0xe0 && b <= 0xff = return $ NInt (fromIntegral b)
            | otherwise = mzero  -- should never happen

  put (PInt y) = put y
  put (NInt y) = put y
  put Nil = putWord8 0xc0
  put (Bool False) = putWord8 0xc2
  put (Bool True) = putWord8 0xc3
  put (Float y) = putWord8 0xca >> putFloat32be y
  put (Double y) = putWord8 0xcb >> putFloat64be y
  put (W8 y) = putWord8 0xcc >> put y
  put (W16 y) = putWord8 0xcd >> putWord16be y
  put (W32 y) = putWord8 0xce >> putWord32be y
  put (W64 y) = putWord8 0xcf >> putWord64be y
  put (I8 y) = putWord8 0xd0 >> put y
  put (I16 y) = putWord8 0xd1 >> putWord16be (fromIntegral y)
  put (I32 y) = putWord8 0xd2 >> putWord32be (fromIntegral y)
  put (I64 y) = putWord8 0xd3 >> putWord64be (fromIntegral y)

  put (Binary (Len n') y)
      | n >= 2^!5  && n < 2^!8  = putWord8 0xc4 >> putWord8 (toW8 n) >> put y
      | n >= 2^!8  && n < 2^!16 = putWord8 0xc5 >> putWord16be (toW16 n) >> put y
      | n >= 2^!16 && n < 2^!32 = putWord8 0xc6 >> putWord32be (toW32 n) >> put y
      | otherwise             = fail "bin: too big"
    where n   = toI n'

  put (Text (Len n') y)
      | n < 2^!5                = putWord8 (0xa0 .|. toW8 n) >> putTxt y
      | n >= 2^!5 && n < 2^!8   = putWord8 0xd9 >> putWord8 (toW8 n) >> putTxt y
      | n >= 2^!8 && n < 2^!16  = putWord8 0xda >> putWord16be (toW16 n) >> putTxt y
      | n >= 2^!16 && n < 2^!32 = putWord8 0xdb >> putWord32be (toW32 n) >> putTxt y
      | otherwise             = fail "str: too big"
    where n   = toI n'
          putTxt = put . encodeUtf8

  put (Array y)
      | n < 16                  = putWord8 (0x90 .|. toW8 n) >> put (V.toList y)
      | n >= 16    && n < 2^!16 = putWord8 0xdc >> putWord16be (toW16 n) >> put (V.toList y)
      | n >= 2^!16 && n < 2^!32 = putWord8 0xdd >> putWord32be (toW32 n) >> put (V.toList y)
      | otherwise = fail "array: too big"
    where n   = V.length y

  put (Map y)
      | n < 16                  = put (0x80 .|. toW8 n) >> putMap y
      | n >= 16   && n < 2^!16  = putWord8 0xde >> putWord16be (toW16 n) >> putMap y
      | n >= 2^!16 && n < 2^!32 = putWord8 0xdf >> putWord32be (toW32 n) >> putMap y
      | otherwise = fail "map: too big"
    where n   = HM.size y
          putMap = put . HM.toList

  put (FixExt (Len n) (Type t) d)
    | n == 1    = putWord8 0xd4 >> putWord8 t >> put d
    | n == 2    = putWord8 0xd5 >> putWord8 t >> put d
    | n == 4    = putWord8 0xd6 >> putWord8 t >> put d
    | n == 8    = putWord8 0xd7 >> putWord8 t >> put d
    | n == 16   = putWord8 0xd8 >> putWord8 t >> put d
    | otherwise = fail $ "put fixext: unknown Len " ++ show n

  put (Ext (Type t) d)
    | n < 2^!8                = putWord8 0xc7 >> putWord8 n8 >> putWord8 t >> put d
    | n >= 2^!8  && n < 2^!16 = putWord8 0xc7 >> putWord16be n16 >> putWord8 t >> put d
    | n >= 2^!16 && n < 2^!32 = putWord8 0xc7 >> putWord32be n32 >> putWord8 t >> put d
    | otherwise = fail $ "put ext: too big with length " ++ show n
      where n = B.length d
            n8  = fromIntegral n
            n16 = fromIntegral n
            n32 = fromIntegral n

-- force Int type for power calculations
(^!) :: Int -> Int -> Int
(^!) = (^)

toW8 :: Integral a => a -> Word8
toW16 :: Integral a => a -> Word16
toW32 :: Integral a => a -> Word32
toW64 :: Integral a => a -> Word64
toW8 = fromIntegral
toW16 = fromIntegral
toW32 = fromIntegral
toW64 = fromIntegral

toI  :: Integral a => a -> Int
toI8 :: Integral a => a -> Int8
toI16 :: Integral a => a -> Int16
toI32 :: Integral a => a -> Int32
toI64 :: Integral a => a -> Int64
toI = fromIntegral
toI8 = fromIntegral
toI16 = fromIntegral
toI32 = fromIntegral
toI64 = fromIntegral

mkExt :: Integral a => Get a -> Get Value
mkExt f = do
  n <- f
  let len = fromIntegral n
  typ <- getWord8
  Ext (Type typ) <$> getBytes len

mkBinary :: Integral a => Get a -> Get Value
mkBinary f = do
  n <- f
  let len = fromIntegral n
  Binary (Len len) <$> getBytes len

mkFixExt :: Int -> Get Value
mkFixExt n = do
  typ <- getWord8
  dat <- getBytes n
  return $ FixExt (Len n) (Type typ) dat -- fixext 2

mkStr :: Integral a => Get a -> Get Value
mkStr f = do
  n <- f
  let len = fromIntegral n
  Text (Len len) <$> (decodeUtf8 <$> getBytes len) -- str 8

mkArray :: Integral a => Get a -> Get Value
mkArray f = do
  _ <- f
  -- let len = fromIntegral n
  -- stream <- getBytes len
  Array <$> get -- array 16

mkMap :: Integral a => Get a -> Get Value
mkMap f = do
  _ <- f
  -- let len = fromIntegral n
  Map <$> get -- map 16
