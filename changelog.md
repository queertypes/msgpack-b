0.2.0.0 (November 29, 2014)

* Implemented serialization for full msgpack spec

0.1.0.0 (November 28, 2014)

* First commit! \o/
